package com.example.xmltest.service.impl;

import com.example.xmltest.model.Users;
import com.example.xmltest.repository.UserRepository;
import com.example.xmltest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.jws.WebService;

@RequiredArgsConstructor
@WebService
@Component
public class UsersSoapService implements UserService {
    private final UserRepository userRepository;
    @Override
    public Users save(Users users) {
        return userRepository.save(users);
    }

    @Override
    public Users get(long id) {
        return userRepository.findById(id).get();
    }
}
