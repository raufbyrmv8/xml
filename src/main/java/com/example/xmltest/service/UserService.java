package com.example.xmltest.service;

import com.example.xmltest.model.Users;

import javax.jws.WebMethod;


public interface UserService {
    // save service
    @WebMethod
    Users save(Users users);

    // find by user
    @WebMethod
    Users get(long id);
}
