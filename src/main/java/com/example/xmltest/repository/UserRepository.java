package com.example.xmltest.repository;

import com.example.xmltest.model.Users;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Users, Long> {
}
