package com.example.xmltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class XmlTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(XmlTestApplication.class, args);
    }

}
