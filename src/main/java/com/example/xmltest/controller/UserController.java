package com.example.xmltest.controller;

import com.example.xmltest.model.Users;
import com.example.xmltest.service.UserService;
import com.example.xmltest.service.impl.UsersSoapService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.jws.WebService;


@Controller
@RequestMapping("/api/users")
public class UserController {
    private final UsersSoapService usersSoapService;
    @Autowired
    public UserController(UsersSoapService usersSoapService) {
        this.usersSoapService = usersSoapService;
    }

    @PostMapping(value = "/save", produces = "application/xml", consumes = "application/xml")
    @ResponseBody
    public Users saveUser(@RequestBody Users users) {
        return usersSoapService.save(users);
    }
    @GetMapping(value = "/{id}", produces = "application/xml")
    @ResponseBody
    public Users getUser(@PathVariable long id) {
        return usersSoapService.get(id);
    }
}
